import autosize from 'autosize';

export default {
    isLiteral: true,
    isEmpty: true,

    bind(el) {
        autosize(el);
        el.style.resize = 'none';
    },

    inserted(el) {
        autosize.update(el);
    },

    componentUpdated(el) {
        autosize.update(el);
    },

    unbind(el) {
        autosize.destroy(el);
    },
};
