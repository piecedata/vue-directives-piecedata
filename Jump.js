import jump from 'jump.js';
import { easeInOutExpo } from './ease';

export default {
    isLiteral: true,
    isEmpty: true,

    bind(el, binding) {
        const options = {
            duration: 400,
            easing: easeInOutExpo,
            offset: 0,
        };

        if (binding.value &&
            Object.prototype.toString.call(binding.value) === '[object Object]') {
            Object.assign(options, binding.value);
        }

        el.addEventListener('click', () => {
            if (!el.hash) {
                return;
            }

            const currentPage = window.location.hash
                ? window.location.href.substr(0, window.location.href.indexOf('#'))
                : window.location.href;
            const nextPage = el.href.substr(0, el.href.indexOf('#'));

            if (currentPage !== nextPage) {
                return;
            }

            const jumpToElement = document.getElementById(el.hash.substring(1));

            if (!jumpToElement) {
                return;
            }

            jump(jumpToElement, options);
        }, false);
    },
};
